import cv2

#(1) False positive - ako stavi okvir, a nije trebao
#(2) False negative - ako ne stavi okvir, a rebao je
#(3) True positive - dobro je prepoznao
#(4) True negative - nije dobro prepoznao

cap = cv2.VideoCapture("videos\\evaluated.avi")
fp = 0
fn = 0
tp = 0
tn = 0

def showFrame(frame, fp, fn, tp, tn):
    font = cv2.FONT_HERSHEY_PLAIN
    frame_now = frame
    frame_now = cv2.putText(frame_now, f"(1) False positive = {fp}", (200 ,20), font, 1, (255, 0, 0), 1, cv2.LINE_AA )
    frame_now = cv2.putText(frame_now, f"(2) False negative = {fn}", (200 ,40), font, 1, (0, 255, 0), 1, cv2.LINE_AA )
    frame_now = cv2.putText(frame_now, f"(3) True positive = {tp}", (200 ,60), font, 1, (255, 0, 255), 1, cv2.LINE_AA )
    frame_now = cv2.putText(frame_now, f"(4) True negative = {tn}", (200 ,80), font, 1, (0, 0, 255), 1, cv2.LINE_AA )
    frame_now = cv2.putText(frame_now, f"    Frame number = {i}", (200 ,100), font, 1, (255, 255, 0), 1, cv2.LINE_AA )
    frame_now = cv2.putText(frame_now, f"    Evaluated frames = {e}", (200 ,120), font, 1, (0, 255, 0), 1, cv2.LINE_AA )
    cv2.imshow("window", frame_now)



if not cap.isOpened():
    print("Error opening video stream or file!")
i = 0
e = 0
while(cap.isOpened()):
    ret, frame = cap.read()
    q = False
    if ret:
        i+=1
        if not i%10==0:
            continue
        
        showFrame(frame, fp, fn, tp, tn)
        while True:
            e+=1
            k = cv2.waitKey()
            if k==49:
                fp+=1
            elif k==50:
                fn+=1
            elif k==51:
                tp+=1
            elif k==52:
                tn+=1
            elif k==27:
                q = True
                break
            elif k==13:
                break
            else:
                e-=1
        if q:
            break
    else:
        break

print(fp, fn, tp, tn, i, e)
cap.release()
cv2.destroyAllWindows()
f = open("log.txt", "w")
f.write("{} {} {} {} {} {}".format(fp, fn, tp, tn, i, e))
f.close()

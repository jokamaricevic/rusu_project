import os
import datetime

import cv2
import keras
import numpy as np
from imageai.Detection import ObjectDetection

execution_path = os.getcwd()
classes = {0 : "cat", 1 : "dog"}
frames_number = 7498
l = frames_number
i = 0
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    if iteration == total: 
        print()

def predict_image(imagein, classifier):
    predict_modified = np.resize(imagein, (64, 64, 3))
    predict_modified = predict_modified / 255
    predict_modified = np.expand_dims(predict_modified, axis=0)
    result = classifier.predict(predict_modified)
    predict = np.argmax(result)
    return predict, result[0][predict]


def load_models():
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath(os.path.join(
        execution_path, "models\\resnet50_coco_best_v2.1.0.h5"))
    detector.loadModel("flash")
    classifier = keras.models.load_model("models/my_model")
    return detector, classifier


def detect_objects(imagein):
    _, detections, detected_objects_image_array = detector.detectObjectsFromImage(input_image=imagein, 
                                input_type='array', display_box=False, minimum_percentage_probability=45,
                                display_object_name=False, display_percentage_probability=False, output_type='array',
                                extract_detected_objects=True, custom_objects=['cat', 'dog'])
    return detections, detected_objects_image_array

def draw_boxes(frame, detections, images):
    for i in range(len(detections)) :
        image, box_points = images[i], detections[i]["box_points"]
        predict, acc = predict_image(image, classifier)
        frame = cv2.rectangle(frame, (box_points[0], box_points[1]), (box_points[2], box_points[3]), (255, 0, 0), 2)
        font = cv2.FONT_HERSHEY_PLAIN
        frame = cv2.putText(frame, f"{classes[predict]} {str(round(acc, 2))}%", 
                (box_points[0], box_points[1]-15), font, 1, (255, 0,0), 1, cv2.LINE_AA )
    return frame

detector, classifier = load_models()
cap = cv2.VideoCapture("videos\\origin.mp4")
if not cap.isOpened():
    print("Error opening video stream or file!")
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
out = cv2.VideoWriter('videos\\output.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 24, (frame_width,frame_height))


printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)

start = datetime.datetime.now()
while(cap.isOpened()):
    ret, frame = cap.read()

    if ret:
        done = datetime.datetime.now() - start
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = f'Complete   Time elapsed: {done}', length = 50)
        i+=1
        detections, images = detect_objects(frame)
        frame = draw_boxes(frame, detections, images)
        cv2.imshow("Frame", frame)
        out.write(frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break
cap.release()
out.release()

cv2.destroyAllWindows()

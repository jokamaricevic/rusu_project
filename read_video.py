import cv2

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

cap = cv2.VideoCapture("videos\\dogsNcats.mp4")

frames_number = 7498
l = frames_number

# Initial call to print 0% progress
printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)

if not cap.isOpened():
    print("Error opening video stream or file!")
i = 0
while(cap.isOpened()):
    ret, frame = cap.read()

    if ret:
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        i+=1
        if i%3 != 0:
            continue
        if cv2.waitKey(75) & 0xFF == ord('q'):
            break
        
        cv2.imshow("Frame", frame)
    else:
        break
print(i)
cap.release()
cv2.destroyAllWindows()